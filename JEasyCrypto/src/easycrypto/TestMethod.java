package easycrypto;

import easycrypto.EasyCryptoAPI.Result;
import easycrypto.EasyCryptoAPI.ResultCode;

public class TestMethod implements CryptoKeyMethod {
	private String key;

	@Override
	public Result encrypt(String toEncrypt) {
		return new Result(ResultCode.ESuccess, key+"-"+toEncrypt);
	}

	@Override
	public Result decrypt(String toDecrypt) {
		return new Result(ResultCode.ESuccess, toDecrypt.replace(key+"-", ""));
	}

	@Override
	public String method() {
		return "test";
	}

	@Override
	public void setKey(String key) {
		this.key = key;
	}

}
