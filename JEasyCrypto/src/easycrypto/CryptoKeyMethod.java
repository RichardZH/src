package easycrypto;

import easycrypto.EasyCryptoAPI.Result;

public interface CryptoKeyMethod extends CryptoMethod {
	/**
    Set Crypt key.
    @param Crypt key.
    */
   public void setKey(final String key);
}
